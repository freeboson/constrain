

/*

    ****************************************************************************
    *                                                                          *
    *                                                                          *
    * Sujeet Akula                                                             *
    * sujeet@freeboson.org                                                     *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    *                                                                          *
    ****************************************************************************

*/


#include "parseutils.hpp"

#include <algorithm>

using namespace std;

ostream& operator<< (ostream &o, const vector<double> &v)
{
	for_each(v.begin(),v.end(),row_printer(&o));
	return o;
}




